#include "wsdd.h"
#include "wsdiscoveryserver.h"

#include "wsdd.nsmap"

WSDiscoveryData::WSDiscoveryData(std::string xaddr, std::string type, std::string scope, std::string endpoint , int metadataVersion)
    : m_xaddr(xaddr), m_type(type), m_scope(scope), m_endpoint(endpoint), m_metadataVersion(metadataVersion)
{
}

int sendHello(soap* server)
{
    WSDiscoveryData* context = static_cast<WSDiscoveryData*>(server->user);

    struct soap* soap = soap_new1(SOAP_IO_UDP);
    printf("call soap_wsdd_Hello\n");
    int res = soap_wsdd_Hello(soap,
                              SOAP_WSDD_ADHOC,      // mode
                              "soap.udp://239.255.255.250:3702",         // address of TS
                              soap_wsa_rand_uuid(soap),                   // message ID
                              NULL,
                              context->m_endpoint.c_str(),
                              context->m_type.c_str(),
                              context->m_scope.c_str(),
                              NULL,
                              context->m_xaddr.c_str(),
                              context->m_metadataVersion);
    if (res != SOAP_OK)
    {
        soap_print_fault(soap, stderr);
    }
    soap_destroy(soap);
    soap_end(soap);

    return soap_wsdd_listen(server, -1000000);
}

int sendBye(soap* server)
{
    WSDiscoveryData* context = static_cast<WSDiscoveryData*>(server->user);

    struct soap* soap = soap_new1(SOAP_IO_UDP);
    printf("call soap_wsdd_Bye\n");
    int res = soap_wsdd_Bye(soap,
                            SOAP_WSDD_ADHOC,      // mode
                            "soap.udp://239.255.255.250:3702",         // address of TS
                            soap_wsa_rand_uuid(soap),                   // message ID
                            context->m_endpoint.c_str(),
                            context->m_type.c_str(),
                            context->m_scope.c_str(),
                            NULL,
                            context->m_xaddr.c_str(),
                            context->m_metadataVersion);
    if (res != SOAP_OK)
    {
        soap_print_fault(soap, stderr);
    }
    soap_destroy(soap);
    soap_end(soap);

    return soap_wsdd_listen(server, -1000000);
}

void wsdd_event_ProbeMatches(struct soap *soap,
                             unsigned int InstanceId,
                             const char *SequenceId,
                             unsigned int MessageNumber,
                             const char *MessageID, const
                             char *RelatesTo,
                             struct wsdd__ProbeMatchesType *matches)
{
}

void wsdd_event_ResolveMatches(struct soap *soap,
                               unsigned int InstanceId,
                               const char *SequenceId,
                               unsigned int MessageNumber,
                               const char *MessageID,
                               const char *RelatesTo,
                               struct wsdd__ResolveMatchType *match)
{
}

void wsdd_event_Hello(struct soap *soap,
                      unsigned int InstanceId,
                      const char *SequenceId,
                      unsigned int MessageNumber,
                      const char *MessageID,
                      const char *RelatesTo,
                      const char *EndpointReference,
                      const char *Types,
                      const char *Scopes,
                      const char *MatchBy,
                      const char *XAddrs,
                      unsigned int MetadataVersion)
{
}

void wsdd_event_Bye(struct soap *soap,
                    unsigned int InstanceId,
                    const char *SequenceId,
                    unsigned int MessageNumber,
                    const char *MessageID,
                    const char *RelatesTo,
                    const char *EndpointReference,
                    const char *Types,
                    const char *Scopes,
                    const char *MatchBy,
                    const char *XAddrs,
                    unsigned int *MetadataVersion)
{
}

soap_wsdd_mode wsdd_event_Resolve(struct soap *soap,
                                  const char *MessageID,
                                  const char *ReplyTo,
                                  const char *EndpointReference,
                                  struct wsdd__ResolveMatchType *match)
{
    WSDiscoveryData* context = static_cast<WSDiscoveryData*>(soap->user);
    if (EndpointReference && (context->m_endpoint == EndpointReference) )
    {
        soap_wsdd_ResolveMatches(soap,
                                 NULL,
                                 soap_wsa_rand_uuid(soap),
                                 MessageID,
                                 ReplyTo,
                                 context->m_endpoint.c_str(),
                                 context->m_type.c_str(),
                                 context->m_scope.c_str(),
                                 NULL,
                                 context->m_xaddr.c_str(),
                                 context->m_metadataVersion);
    }
    return SOAP_WSDD_ADHOC;
}

soap_wsdd_mode wsdd_event_Probe(struct soap *soap,
                                const char *MessageID,
                                const char *ReplyTo,
                                const char *Types,
                                const char *Scopes,
                                const char *MatchBy,
                                struct wsdd__ProbeMatchesType *matches)
{
    WSDiscoveryData* context = static_cast<WSDiscoveryData*>(soap->user);
    soap_wsdd_init_ProbeMatches(soap,matches);
    soap_wsdd_add_ProbeMatch(soap,
                             matches,
                             context->m_endpoint.c_str(),
                             context->m_type.c_str(),
                             context->m_scope.c_str(),
                             NULL, context->m_xaddr.c_str(),
                             context->m_metadataVersion);
    soap_wsdd_ProbeMatches(soap, NULL, soap_wsa_rand_uuid(soap) , MessageID, ReplyTo, matches);
    return SOAP_WSDD_ADHOC;
}

