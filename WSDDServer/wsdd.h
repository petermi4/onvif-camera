#ifndef WSDD_H
#define WSDD_H

#include <string>

#include "wsddapi.h"
#include "wsaapi.h"

class WSDiscoveryData
{
public:
    WSDiscoveryData(std::string xaddr, std::string type, std::string scope, std::string endpoint, int metadataVersion);
    std::string m_xaddr;
    std::string m_type;
    std::string m_scope;
    std::string m_endpoint;
    int m_metadataVersion;
};

int sendHello(soap* server);

int sendBye(soap* server);

#endif // WSDD_H
