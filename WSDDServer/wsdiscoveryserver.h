#ifndef WSDISCOVERYSERVER_H
#define WSDISCOVERYSERVER_H

#include <QObject>
#include <QFuture>
#include <QtConcurrent>
#include <iostream>

class WSDiscoveryServer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString xaddr READ xaddr WRITE setXaddr NOTIFY xaddrChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString scope READ scope WRITE setScope NOTIFY scopeChanged)
    Q_PROPERTY(QString endpoint READ endpoint WRITE setEndpoint NOTIFY endpointChanged)
    Q_PROPERTY(int metadataVersion READ metadataVersion WRITE setMetadataVersion NOTIFY metadataVersionChanged)
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
public:
    explicit WSDiscoveryServer(QString xaddr,
                               QString type,
                               QString scope = "",
                               QString endpoint = "urn:endpoint",
                               int metadataVersion = 0,
                               QObject *parent = nullptr);

    ~WSDiscoveryServer();

    QString xaddr() const;
    QString type() const;
    QString scope() const;
    QString endpoint() const;
    int metadataVersion() const;
    bool running() const;

    void setXaddr(const QString val);
    void setType(const QString val);
    void setScope(const QString val);
    void setEndpoint(const QString val);
    void setMetadataVersion(const int val);

public slots:
    void start();
    void stop();

signals:
    void xaddrChanged();
    void typeChanged();
    void scopeChanged();
    void endpointChanged();
    void metadataVersionChanged();
    void runningChanged();

private:
    QString m_xaddr;
    QString m_type;
    QString m_scope;
    QString m_endpoint;
    int m_metadataVersion;
    bool m_running;

    void runInternal();

    QFuture<void> m_runFuture;
};

#endif // WSDISCOVERYSERVER_H
