#include "wsdiscoveryserver.h"

WSDiscoveryServer::WSDiscoveryServer(QString xaddr, QString type, QString scope, QString endpoint, int metadataVersion, QObject *parent)
    : QObject{parent},
      m_xaddr(xaddr),
      m_type(type),
      m_scope(scope),
      m_endpoint(endpoint),
      m_metadataVersion(metadataVersion),
      m_running(false)
{
}

WSDiscoveryServer::~WSDiscoveryServer()
{
}

QString WSDiscoveryServer::xaddr() const
{
    return m_xaddr;
}

QString WSDiscoveryServer::type() const
{
    return m_type;
}

QString WSDiscoveryServer::scope() const
{
    return m_scope;
}

QString WSDiscoveryServer::endpoint() const
{
    return m_endpoint;
}

int WSDiscoveryServer::metadataVersion() const
{
    return m_metadataVersion;
}

bool WSDiscoveryServer::running() const
{
    return m_running;
}

void WSDiscoveryServer::setXaddr(const QString val)
{
    if (m_running || m_xaddr == val) return;
    m_xaddr = val;
    emit xaddrChanged();
}

void WSDiscoveryServer::setType(const QString val)
{
    if (m_running || m_type == val) return;
    m_type = val;
    emit typeChanged();
}

void WSDiscoveryServer::setScope(const QString val)
{
    if (m_running || m_scope == val) return;
    m_scope = val;
    emit scopeChanged();
}

void WSDiscoveryServer::setEndpoint(const QString val)
{
    if (m_running || m_endpoint == val) return;
    m_endpoint = val;
    emit endpointChanged();
}

void WSDiscoveryServer::setMetadataVersion(const int val)
{
    if (m_running || m_metadataVersion == val) return;
    m_metadataVersion = val;
    emit metadataVersionChanged();
}

void WSDiscoveryServer::start()
{
    if (m_running) return;
    m_running = true;
    m_runFuture = QtConcurrent::run(&WSDiscoveryServer::runInternal, this);
    emit runningChanged();
}

void WSDiscoveryServer::stop()
{
    if (!m_running) return;
    m_running = false;
    m_runFuture.waitForFinished();
    emit runningChanged();
}

#include "wsdd.h"
void WSDiscoveryServer::runInternal()
{
    struct soap* server = soap_new1(SOAP_IO_UDP);
    server->bind_flags=SO_REUSEADDR;
    if (!soap_valid_socket(soap_bind(server, NULL, 3702, 1000))) {
        soap_print_fault(server, stderr);
        soap_destroy(server);
        soap_end(server);
        m_running = false;
        emit runningChanged();
        return;
    }

    ip_mreq mcast;
    inet_pton(AF_INET, "239.255.255.250", &mcast.imr_multiaddr.s_addr);
    mcast.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(server->master, IPPROTO_IP, IP_ADD_MEMBERSHIP, reinterpret_cast<const char*>(&mcast), sizeof(mcast)) < 0) {
        std::cerr << "Failed to join multicast group: " << std::strerror(errno) << std::endl;
        soap_destroy(server);
        soap_end(server);
        m_running = false;
        emit runningChanged();
        return;
    }

    WSDiscoveryData data(m_xaddr.toStdString(),
                         m_type.toStdString(),
                         m_scope.toStdString(),
                         m_endpoint.toStdString(),
                         m_metadataVersion);

    server->user = static_cast<void*>(&data);

    sendHello(server);
    while (m_running) {
        soap_wsdd_listen(server, -1000);
    }
    sendBye(server);


    soap_destroy(server);
    soap_end(server);
}

